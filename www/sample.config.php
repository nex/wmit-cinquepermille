<?php

  // Debug
  $debug = true;

  // tokens
  $tokenfile = '../tokens.txt';
  $maxtokens = 20;

  // Webform settings
  $webformendpoint = '';

  // SMS settings
  $volauser = '';
  $volapwd = '';
  $from = 'WikimediaIT';
  $msg = "Ciao, ricordati di sostenere la conoscenza libera scrivendo il codice fiscale 94039910156 nella tua dichiarazione dei redditi. Grazie da Wikimedia Italia!";

 ?>
