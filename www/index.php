<?php

header('Cache-Control: max-age=86400');
?>

<!DOCTYPE html>

<html lang="it" class="">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0,">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="css/wp.css">
    <link rel="stylesheet" href="css/emerald.css">
    <meta property="og:description" content="Inserisci il Cod. fiscale 94039910156 nella tua Dichiarazione dei Redditi per destinare il 5x1000 alla conoscenza libera.">



<!-- Matomo Tag Manager -->

	<script type="text/javascript">
	var _mtm = window._mtm = window._mtm || [];
	_mtm.push({'mtm.startTime': (new Date().getTime()), 'event': 'mtm.Start'});
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.src='https://matomo.wikimedia.it/js/container_JTGCNdWJ.js'; s.parentNode.insertBefore(g,s);
	</script>

	<!-- End Matomo Tag Manager -->

	<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>

	<title>Home - Destina il tuo 5 per mille a Wikimedia!</title>
	<meta name="description" content="Dai alla conoscenza libera un nuovo nome, il tuo. Destina il 5x1000 a Wikimedia Italia con il codice fiscale 94039910156">
	<meta property="og:locale" content="it_IT">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Home - Destina il tuo 5 per mille a Wikimedia!">
	<meta property="og:description" content="Dai alla conoscenza libera un nuovo nome, il tuo. Destina il 5x1000 a Wikimedia Italia con il codice fiscale 94039910156">
	<meta property="og:url" content="https://cinquepermille.wikimedia.it/">
	<meta property="og:site_name" content="Destina il tuo 5 per mille a Wikimedia!">
	<meta property="article:modified_time" content="2022-05-10T08:15:10+00:00">
	<meta name="twitter:card" content="summary_large_image">
	<script type="application/ld+json" class="yoast-schema-graph">
  {
    "@context": "https://schema.org",
    "@graph": [
      {
        "@type": "Organization",
        "@id": "https://cinquepermille.wikimedia.it/#organization",
        "name": "Wikimedia Italia",
        "url": "https://cinquepermille.wikimedia.it/",
        "sameAs": [],
        "logo": {
          "@type": "ImageObject",
          "inLanguage": "it-IT",
          "@id": "https://cinquepermille.wikimedia.it/#/schema/logo/image/",
          "url": "https://cinquepermille.wikimedia.it/wp-content/uploads/2022/05/logo-wikimedia-square-2021_h120-1.png",
          "contentUrl": "https://cinquepermille.wikimedia.it/wp-content/uploads/2022/05/logo-wikimedia-square-2021_h120-1.png",
          "width": 120,
          "height": 120,
          "caption": "Wikimedia Italia"
        },
        "image": {
          "@id": "https://cinquepermille.wikimedia.it/#/schema/logo/image/"
        }
      },
      {
        "@type": "WebSite",
        "@id": "https://cinquepermille.wikimedia.it/#website",
        "url": "https://cinquepermille.wikimedia.it/",
        "name": "Destina il tuo 5 per mille a Wikimedia!",
        "description": "Sostieni la cultura, dai il 5 per 1000 a Wikimedia Italia; scrivi 94039910156!",
        "publisher": {
          "@id": "https://cinquepermille.wikimedia.it/#organization"
        },
        "inLanguage": "it-IT"
      },
      {
        "@type": "WebPage",
        "@id": "https://cinquepermille.wikimedia.it/#webpage",
        "url": "https://cinquepermille.wikimedia.it/",
        "name": "Home - Destina il tuo 5 per mille a Wikimedia!",
        "isPartOf": {
          "@id": "https://cinquepermille.wikimedia.it/#website"
        },
        "about": {
          "@id": "https://cinquepermille.wikimedia.it/#organization"
        },
        "datePublished": "2019-03-20T16:07:25+00:00",
        "dateModified": "2022-05-10T08:15:10+00:00",
        "description": "Dai alla conoscenza libera un nuovo nome, il tuo. Destina il 5x1000 a Wikimedia Italia con il codice fiscale 94039910156",
        "breadcrumb": {
          "@id": "https://cinquepermille.wikimedia.it/#breadcrumb"
        },
        "inLanguage": "it-IT",
        "potentialAction": [
          {
            "@type": "ReadAction",
            "target": [
              "https://cinquepermille.wikimedia.it/"
            ]
          }
        ]
      },
      {
        "@type": "BreadcrumbList",
        "@id": "https://cinquepermille.wikimedia.it/#breadcrumb",
        "itemListElement": [
          {
            "@type": "ListItem",
            "position": 1,
            "name": "Home"
          }
        ]
      }
    ]
  }

  </script>


<script type="text/javascript" src="js/jquery.min.js" id="jquery-core-js"></script>
<link rel="icon" type="image/x-icon" href="assets/wmi-favicon.ico">
<link rel="shortlink" href="https://cinquepermille.wikimedia.it/">


<?php


  include_once 'config.php';


  function generateRandomString($length = 16) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[random_int(0, $charactersLength - 1)];
  }
  return $randomString;
  }

  $validationToken = generateRandomString($length = 16);
  $hashedValidationToken = md5($validationToken);

  if ($debug ){
    echo '<br/>';
    echo '$validationToken: '.$validationToken.'<br/>';
    echo '$hashedValidationToken: '.$hashedValidationToken.'<br/>';
  }



  $tokens = explode(PHP_EOL, file_get_contents($tokenfile));
  $tokensCount = count($tokens);

  if(!is_file($tokenfile)){
    if ($debug ){
      echo 'Token File not exists in '.$tokenfile.'<br/>';
    }
    file_put_contents($tokenfile, $validationToken.PHP_EOL);
  }else{
    if ($debug ){
      echo 'Token File Exists in '.$tokenfile.'<br/>';
    }


  }

  if ($debug){
    echo '<br/>Number of tokens: '.$tokensCount.'<br/>';
    echo '<br/>';
    echo 'OLD Tokens:';
    echo '<pre>';
    print_r($tokens);
    echo '</pre>';
  }


  foreach ($tokens as $token_key => $token_value) {
    if($token_value == ''){
      unset($tokens[$token_key]);
    }
  }

  if ($tokensCount >= $maxtokens) {
    if ($debug){
      echo 'Too much tokens!<br/>';
    }
    $tokens = array_slice($tokens, 2);
  }

  array_push($tokens,$validationToken);

  if ($debug ){
    echo '<br/>';
    echo 'NEW  Tokens:';
    echo '<pre>';
    print_r($tokens);
    echo '</pre>';
  }

  $newfile = '';
  foreach ($tokens as $token_key => $token_value) {
    $newfile .= $token_value.PHP_EOL;
  }



  file_put_contents($tokenfile, $newfile);

  echo '<meta name="validationtoken" content="'.$hashedValidationToken.'">';
  //phpinfo();

  echo '<meta name="webformendpoint" content="'.$webformendpoint.'">'
 ?>


</head>

<body>

    <div id="page" class="page">
    <header class="header" :class="['header', headerBg, navClass]">
      <p class="logo">
        <a href="https://cinquepermille.wikimedia.it/#">
          <img id="homelogo" src="assets/Wikimedia_Italia-logo-black-horizontal.svg" alt="Wikimedia Logo">
        </a>
      </p>
      <p class="menu">
        <a  @click.prevent="toggleMenu" href="#"><span>Menu</span></a>
      </p>
      <nav class="nav">
      <ul>
        <li><a href="#come-donare" class="nav__item"><span>Come destinare <em>il 5x1000</em></span></a></li>
        <li><a href="#quanto-vale" class="nav__item"><span>Quanto vale <em>il tuo 5x1000</em></span></a></li>
        <!-- <li><a href="#perche-wikimedia" class="nav__item"><span>Perché Wikimedia Italia</span></a></li> -->
        <!-- <li><a href="#"><span>Condividi sui tuoi social</span></a></li> -->
      </ul>
      </nav>
    </header>


  <section class="intro">
    <h1>Dai alla conoscenza libera un nuovo nome. <em>Il tuo. Con il tuo 5x1000</em></h1>
  </section>

  <div class="more">
    <p>Destina il 5x1000 a Wikimedia Italia con il codice fiscale

      <span class="copy_cf" title="Copia il codice fiscale">
        <em>94039910156</em>
      </span>


        <span class="copy_btn copy_cf" title="Copia il codice fiscale">
          Copia il CF
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
            <path d="M208 0H332.1c12.7 0 24.9 5.1 33.9 14.1l67.9 67.9c9 9 14.1 21.2 14.1 33.9V336c0 26.5-21.5 48-48 48H208c-26.5 0-48-21.5-48-48V48c0-26.5 21.5-48 48-48zM48 128h80v64H64V448H256V416h64v48c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V176c0-26.5 21.5-48 48-48z"/>
          </svg>
        </span>

    </p>

  </div>
  <section class="slideshow">
    <div class="slideshow__aux swiper-container swiper-container-initialized swiper-container-horizontal">
      <ul class="swiper-wrapper" style="transition-duration: 0ms; transform: translate3d(-2634px, 0px, 0px);">
        <li class="swiper-slide" data-swiper-slide-index="2" style="width: 878px;">
          <img alt="Wikimedia Italia" src="assets/slide01.jpg">
        </li>
        <li class="swiper-slide" data-swiper-slide-index="0" style="width: 878px;">
          <img alt="Wikimedia Italia" src="assets/slide02.jpg">
        </li>
        <li class="swiper-slide" data-swiper-slide-index="1" style="width: 878px;">
          <img  alt="Wikimedia Italia" src="assets/slide03.jpg">
        </li>

      </ul>
      <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
    </div>
  </section>

  <section class="reminder">
    <div class="reminder__content">
      <div class="reminder__form">
        <h2>Invia un promemoria</h2>


        <div class="reminder__tabs">
          <ul>
            <li><span class="formtab active" data-activeFormId="formEmail" class="active">Email</span></li>
            <li><span class="formtab" data-activeFormId="formSMS" class="">SMS</span></li>
            <li><a href="https://web.whatsapp.com/send?text=Ciao,%20ricordati%20di%20sostenere%20la%20conoscenza%20libera%20scrivendo%20il%20codice%20fiscale%2094039910156&amp;nbsp;nella%20tua%20dichiarazione%20dei%20redditi.%20Grazie!%20https%3A%2F%2Fcinquepermille.wikimedia.it%2F" target="_blank">Whatsapp</a></li>
            <li><a href="https://t.me/share?url=https%3A%2F%2Fcinquepermille.wikimedia.it%2F&amp;text=Ciao%2C%20ricordati%20di%20sostenere%20la%20conoscenza%20libera%20scrivendo%20il%20codice%20fiscale%2094039910156%20nella%20tua%20dichiarazione%20dei%20redditi.%20Grazie%20da%20Wikimedia%20Italia%21" target="_blank">Telegram</a></li>
          </ul>
        </div>
        <div class="reminder__form__content">

          <div class="formwrapperemail formwrapper active" id="formEmail">
            <div class="webform_form reminder__form__2">
              <div class="webform_content">
              </div>
            </div>
          </div>

          <div class="formwrappersms formwrapper" id="formSMS" style="display: none;">
            <div class="webform_form reminder__form__2">
              <div class="webform_content">
              </div>
            </div>
          </div>

        </div>
      </div>


    </div>
  </section><!-- /.reminder enable -->

  <section id="come-donare" class="how-to scroll-item box">

    <div class="how-to__intro">
      <div class="how-to__intro__content">
        <h2>Come destinare il 5x1000 a Wikimedia Italia</h2>
        <p>Ti basta firmare e inserire il <br>
            <strong>Codice Fiscale
            <span class="copy_cf" title="Copia il codice fiscale">
                <em>94039910156</em>
            </span>
            </strong>
            <span class="copy_btn copy_cf"  title="Copia il codice fiscale">
              Copia il CF
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                <path d="M208 0H332.1c12.7 0 24.9 5.1 33.9 14.1l67.9 67.9c9 9 14.1 21.2 14.1 33.9V336c0 26.5-21.5 48-48 48H208c-26.5 0-48-21.5-48-48V48c0-26.5 21.5-48 48-48zM48 128h80v64H64V448H256V416h64v48c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V176c0-26.5 21.5-48 48-48z"/>
              </svg>
            </span>



          <br>
          nella tua dichiarazione dei redditi.</p>
      </div>
    </div> <!-- /.how-to__intro -->

    <div class="how-to__example">
      <span class="copy_cf" title="Copia il codice fiscale">
        <img alt="Destina il 5x1000 a Wikimedia Italia" src="assets/example.svg">
        <img alt="Destina il 5x1000 a Wikimedia Italia" src="assets/example-2.svg">
      </span>
    </div> <!-- /.how-to__example -->

    <div class="how-to__content">
      <h3>Che dichiarazione presenti?</h3>
      <ul>
        <li class="howtoli" data-howto="howto730" class="active"><span>Modello <em>730</em></span></li>
        <li class="howtoli" data-howto="howtounico"><span>Redditi <em>(ex Unico)</em></span></li>
        <li class="howtoli" data-howto="hownon"><span>Non la <em>presento</em></span></li>
      </ul>
    </div> <!-- /.how-to__content -->

    <div class="how-to__items">
      <ul>
        <li class="howtoitemli" id="howto730" class="active">
          Firma e inserisci il codice fiscale nell’apposito riquadro che trovi nel modulo della tua dichiarazione dei redditi o nella dichiarazione precompilata online.La scadenza per l’invio è fissata al <strong>30 settembre</strong>.
        </li>
        <li class="howtoitemli" id="howtounico">
          Anche per nel modello <strong>Redditi</strong> firma e inserisci il codice fiscale nell’apposito riquadro della dichiarazione. La scadenza per l’invio del modello <strong>Redditi</strong> è fissata al <strong>30 settembre</strong>.
        </li>
        <li class="howtoitemli" id="hownon">
          Puoi utilizzare la Certificazione Unica (CU) o il Modello Redditi (ex Unico). Firma e compila il riquadro del 5x1000 nella "Scheda per la scelta della destinazione dell'8 per mille, del 5 per mille e del 2 per mille dell'IRPEF" e consegnalo alle poste, a un CAF o al tuo commercialista in busta chiusa. La scadenza per l’invio è fissata al <strong>30 settembre</strong>.
        </li>
      </ul>
    </div> <!-- /.how-to__items -->
  </section><!-- /#come-donare -->



  <section id="quanto-vale" class="examples scroll-item box">
    <h2>Cosa possiamo fare grazie al tuo 5x1000</h2>
    <div class="examples__content">

      <div class="examples__item">
        <h3>Dichiari 15.000 €?<br>vale 30 €</h3>
        <img alt="Permetti a un nostro volontario di rendere accessibile a tutti online il patrimonio di una biblioteca o un museo" src="assets/30_euro_a.jpg ">
        <p>
          Permetti a un nostro volontario di rendere accessibile a tutti online il patrimonio di una biblioteca o un museo e arricchisci così le voci in Wikipedia.
        </p>
      </div>

      <div class="examples__item">
        <h3>Dichiari 25.000 €?<br>Vale 40 €</h3>
        <img alt="Formi un insegnante all'utilizzo di Wikipedia" src="assets/40_euro.jpg">
        <p>
          Formi un insegnante all'utilizzo di Wikipedia, uno strumento innovativo da utilizzare a scuola, per coinvolgere gli studenti con compiti concreti.
        </p>
      </div>

      <div class="examples__item">
        <h3>Dichiari 35.000 €?<br>Vale 50 €</h3>
        <img alt="Contribuisci a sostenere le iniziative dei volontari di Wikipedia e OpenStreetMap" src="assets/50_euro.jpg">
        <p>
          Contribuisci a sostenere le iniziative dei volontari di Wikipedia e OpenStreetMap, online e offline. Per aumentare la conoscenza libera.
        </p>
      </div>
    </div>
  </section>



  <section id="perche-wikimedia" class="about-us scroll-item box">

    <!--

    <div class="about-us__intro">
      <h2>Chi è Wikimedia Italia?</h2>
      <p>
        Da più di dieci anni Wikimedia Italia opera per garantire l'accesso libero e la condivisione della conoscenza.<br> <br> <a href="https://www.wikimedia.it/" target="_blank" rel="noopener">Scopri di più</a>
      </p>
    </div>
    <div class="about-us__video">
      <div class="about-us__video__container">
        <video width="320" height="240" poster="https://cinquepermille.wikimedia.it/wp-content/uploads/2019/04/poster.jpg" preload="none" controls="controls">
          <source src="https://upload.wikimedia.org/wikipedia/commons/8/8a/Spot_tv_Wikimedia_5x1000.webm" type="video/webm">
          <source src="https://cinquepermille.wikimedia.it/wp-content/themes/wikimedia/assets/video/Spot_tv_Wikimedia_5x1000_09.05.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
      </div>
    </div>
    -->

    <!--
    <div class="about-us__content">
      <h3>Scopri le storie nate grazie a Wikimedia Italia</h3>
      <div class="about-us__stories">
        <div class="about-us__stories__aux swiper-container swiper-container-initialized swiper-container-horizontal">

          <div class="about-us__stories__aux__2 swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">

            <div class="about-us__item swiper-slide swiper-slide-active" style="width: 571px;">
              <div class="about-us__item__aux">
                <img src="assets/1.png" srcset="https://cinquepermille.wikimedia.it/wp-content/uploads/2019/04/1@2x.png 2x">
                <p>
                  <strong>Francesco</strong>
                  Grazie a una foto scattata ai Trepponti a Comacchio, ha vinto Wiki Loves Monuments 2017. Il contest raccoglie oltre 125.000 scatti di monumenti italiani per illustrare le voci di Wikipedia.
                </p>
              </div>
            </div>


            <div class="about-us__item swiper-slide swiper-slide-next" style="width: 571px;">
              <div class="about-us__item__aux">
                <img src="assets/2.png" srcset="https://cinquepermille.wikimedia.it/wp-content/uploads/2019/04/2@2x.png 2x">
                <p>
                  <strong>Linda</strong>
                  I suoi figli adorano Lady Hawke, il film fantasy girato in Abruzzo. Attraverso una semplice ricerca su Wikipedia, hanno scoperto i luoghi delle riprese e sono partiti insieme per visitarli.
                </p>
              </div>
            </div>


            <div class="about-us__item swiper-slide" style="width: 571px;">
              <div class="about-us__item__aux">
                <img src="assets/3.png" srcset="https://cinquepermille.wikimedia.it/wp-content/uploads/2019/04/3@2x.png 2x">
                <p>
                  <strong>Rossella</strong>
                  Con il progetto Wiki4MediaFreedom di OBC Transeuropa ha modificato e creato ex novo oltre 500 voci Wikipedia sul tema della libertà di stampa. Mentre con Wiki4Refugees ha coinvolto un gruppo di richiedenti asilo nella traduzione di voci dall'italiano alle loro lingue.
                </p>
              </div>
            </div>


          </div>


          <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
        </div>
      </div>


      <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
        <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span>
      </div>
    </div>
    --->
  </section>



  <footer class="footer box">
    <div class="footer__main">
      <h4>
        <strong>Wikimedia Italia</strong>Associazione per la diffusione della conoscenza libera
      </h4>

      <p>
        via Bergognone, 34 - 20144 Milano (MI)
        <br>P.Iva 05599740965 - CF 94039910156
        <br>Tel. 039 5962256 - Fax 039 9462394
        <br> <a href="mailto:segreteria@wikimedia.it">segreteria@wikimedia.it</a>
      </p>
    </div>

    <div class="footer__other-info">
      <ul class="footer__other-info__list">
        <li><a target="_blank" href="https://www.wikimedia.it/privacy/">Informativa sulla privacy</a></li>
      </ul>

      <h4>Seguici su</h4>
      <ul class="footer__other-info__social">
        <li><a href="https://www.facebook.com/Wikimedia.Italia/" target="_blank"><img alt="Wikimedia facebook" src="assets/1.svg" alt="facebook"></a></li>
        <li><a href="https://twitter.com/WikimediaItalia" target="_blank"><img alt="Wikimedia twitter" src="assets/2.svg" alt="twitter"></a></li>
        <li><a href="https://www.youtube.com/user/wikimediaitalia" target="_blank"><img alt="Wikimedia youtube" src="assets/3.svg" alt="youtube"></a></li>
        <li><a href="https://www.instagram.com/wikimediaitalia/" target="_blank"><img alt="Wikimedia instagram" src="assets/Instagram.svg" alt="instagram" style="width: 30px;"></a></li>
      </ul>

      <div class="footer__other-info__content">
        <p>Tutti i contenuti del sito sono disponibili con licenza <span style="text-decoration: underline;"><a href="https://creativecommons.org/licenses/by-sa/3.0/it/">CC BY-SA 3.0</a></span> salvo diversamente specificato</p>

        <p>
        Foto Slideshow: <a target="_blank" href="https://upload.wikimedia.org/wikipedia/commons/3/38/Evento_finale_Archeowiki_16.JPG">1</a> di Niccolò Caranti, <a target="_blank" href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>; <a target="_blank" href="https://upload.wikimedia.org/wikipedia/commons/f/f7/SOTM_2019_%2848769514531%29.jpg">2</a> di Thomas Skowron from Cologne, Germany, <a target="_blank" href="https://creativecommons.org/licenses/by/2.0/">CC BY 2.0</a>; <a target="_blank" href="https://upload.wikimedia.org/wikipedia/commons/5/5c/Visitcomo_wikigita_finale_como_2017.jpg">3</a> di Roberto Colombo, <a target="_blank" href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>, da Wikimedia Commons
        </p>

        <p>
        Foto "Cosa possiamo fare grazie al tuo 5x1000"
        <a target="_blank" href="https://commons.wikimedia.org/wiki/File:Edit-a-thon_in_Turin,_Museo_Egizio,_April_2024_153.jpg">1</a>
        di Domenico Conte
        <a target="_blank"  href="https://creativecommons.org/licenses/by-sa/4.0/deed.it">CC BY-SA 4.0</a>,

        <a target="_blank" href="https://commons.wikimedia.org/wiki/File:Facce_da_laboratorio.jpg">2</a>
        di Silviaelzi
        <a target="_blank" href="https://creativecommons.org/licenses/by/4.0/deed.it">CC BY 4.0</a>,


        <a target="_blank" href="https://commons.wikimedia.org/wiki/File:State_of_the_Map_2024.jpg">3</a>
        di Anisa Kuci
        <a target="_blank" href="https://creativecommons.org/licenses/by/4.0/deed.it">CC BY 4.0</a>

        </p>

        <p>Nota: i fondi raccolti con il cinque per mille sono destinati unicamente a Wikimedia Italia e nulla verrà condiviso con Wikimedia Foundation, Inc. Wikimedia Italia è un’associazione di promozione sociale registrata in Italia, totalmente indipendente da Wikimedia Foundation, Inc. La donazione è anonima: Wikimedia Italia non può venire a conoscenza di nessuna informazione relativa al contribuente (neanche nome e cognome).</p>
      </div>
    </div>
  </footer>
</div>


<div class="copied_warning_wrapper">
  <div class="copied_warning">

    <h3>Codice fiscale copiato negi appunti</h3>
  </div>
</div>



<script src="js/vue.js"></script>
<script src="js/main.js"></script>


<script type="text/javascript" id="twentyseventeen-skip-link-focus-fix-js-extra">
/* <![CDATA[ */
var twentyseventeenScreenReaderText = {"quote":"<svg class=\"icon icon-quote-right\" aria-hidden=\"true\" role=\"img\"> <use href=\"#icon-quote-right\" xlink:href=\"#icon-quote-right\"><\/use> <\/svg>"};
/* ]]> */
</script>

<script type="text/javascript" src="js/skip-link-focus-fix.js" id="twentyseventeen-skip-link-focus-fix-js"></script>
<script type="text/javascript" src="js/global.js" id="twentyseventeen-global-js"></script>
<script type="text/javascript" src="js/jquery.scrollTo.js" id="jquery-scrollto-js"></script>
<script type="text/javascript" src="js/webform.js" id="jquery-scrollto-js"></script>



</body>
</html>
