jQuery(function($){

  // Webform 1
  var WebformEndPoint01 =  $("meta[name='webformendpoint']").attr("content");
  var WebformID01 = '5x1000mail';
  var WebformWrapper01 = '.formwrapperemail';
  var SendSMS01 = false;

  // Webform 2
  var WebformEndPoint02 = $("meta[name='webformendpoint']").attr("content");
  var WebformID02 = '5x1000sms';
  var WebformWrapper02 = '.formwrappersms';
  var SendSMS02 = true;

  var debug = false;

 $( document ).ready(function() {

   getCiviCrmForm(WebformEndPoint01,WebformID01,WebformWrapper01,SendSMS01);
   getCiviCrmForm(WebformEndPoint02,WebformID02,WebformWrapper02,SendSMS02);

   checkRequiredFields('.formwrapper');

   $( "body" ).on( "click tap", ".webformPostMeNow", function() {
     var wrapper_id = $(this).parents('form').attr('id');
     wrapper_id = '#'+wrapper_id;

    postCiviCrmForm(wrapper_id);
    $(wrapper_id).html('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');

   });

   $(document).on('change','.webform_form input',function () {
     checkRequiredFields('.formwrapper');
   });
});




function getCiviCrmForm(endpoint,WebformID,WebformWrapper,SendSMS){

 var endpointGet = endpoint+'webform_rest/'+WebformID+'/fields';
 var endpointPost = endpoint+'webform_rest/submit';
 //var endpointGet = 'https://crmdev.wikimedia.it/webform/webform_rest';
 if (debug){
   console.log('getCiviCrmForm - form id: '+WebformID);
   console.log('getCiviCrmForm - endpointGet: '+endpointGet);
   console.log('getCiviCrmForm - endpointPost: '+endpointPost);
 }

 // Assign handlers immediately after making the request,
 // and remember the jqxhr object for this request
 var jqxhr = $.get( endpointGet, function(data) {
    if (debug){
      if (debug){
        console.log( "ajax get success" );
      }
    }
   })
   .done(function(data) {
     //console.log( "second success",data );
     renderCiviCrmForm(data,WebformID,endpointPost,WebformWrapper,SendSMS);
   })
   .fail(function(data) {
     if (debug){
       console.log("error: ",data );
    }
   })
   .always(function(data) {
     if (debug){
       console.log("finished: ",data );
     }
   });

 };


 function renderCiviCrmForm(data,WebformID,endpointPost,WebformWrapper,SendSMS){

   var tokenValue =  $("meta[name='validationtoken']").attr("content");
   if (debug){
     console.log('tokenValue',tokenValue);
     console.log('renderCiviCrmForm');
   };


   output = "";
   output += "<form id='webform-"+WebformID+"' class='webform_form' data-post-url='"+endpointPost+"' data-form-id='"+WebformID+"' data-sendsms='"+SendSMS+"'>";
   $.each( data, function( key, value ) {

     if (value['#type'] == 'checkboxes' || value['#type'] == 'radios'){
       typeclass= ' multiplevalues';
     }
     else{
       typeclass= ' singlevalue';
     }

     output += "<div class='form-item field "+value['#webform_key']+typeclass+"'>";
     output += renderCiviCrmField(value,WebformID);
     output += "</div>";
   });

   output += "<div class='form-item field'><input type='hidden' name='token' value='"+tokenValue+"'></div>";
   output += "</form>";

  if (debug){
    console.log(output);
  }

   $(WebformWrapper+' .webform_content').html(output);
   checkRequiredFields('.formwrapper');
 };

 function renderCiviCrmField(field,WebformID){
   if (debug){
     console.log(field);
   }
   is_required = '';
   is_required_label = '';
   if (field['#required'] == true){
     is_required = "required='required' required";
     is_required_label = '*';
   }
   output = '';
   if (field['#type'] == 'email'){
     output += "<label>"+field['#title']+is_required_label+"</label>";
     output += "<input class='field input' type='email' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
     output += "<div class='error'></div>";
   }

   if (field['#type'] == 'textfield'){
     output += "<label>"+field['#title']+is_required_label+"</label>";
     output += "<input class='field input' type='text' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
     output += "<div class='error'></div>";
   }

   if (field['#type'] == 'hidden'){
     output += "<input class='field input' type='hidden' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
   }

   if (field['#type'] == 'civicrm_contact'){
     output += "<input class='field input' type='hidden' name='"+field['#webform_key']+"' placeholder='"+field['#placeholder']+"' "+is_required+">";
   }

   if (field['#type'] == 'checkbox'){
     output += "<div class='checkboxwrapper'>";
     output += "<div class='checkboxitem'>";
     output += "<label for='"+field['#webform_key']+WebformID+"'>"+field['#description']+is_required_label+"</label>";
     output += "<input id='"+field['#webform_key']+WebformID+"' class='field input' name='"+field['#webform_key']+"' type='checkbox' value='1' "+is_required+">";
     output += "</div>";
     output += "<div class='error'></div>";
     output += "</div>";
   }

   if (field['#type'] == 'checkboxes'){
    output += "<div class='checkboxwrapper'>";
    output += "<h4 class='checkboxes_title'>"+field['#title']+is_required_label+"</h4>";
    output += "<p class='checkboxes_description'>"+field['#description']+"</p>";
     $.each(field['#options'], function (key,value) {
        output += "<div class='checkboxitem'>";
        output += "<label for='"+field['#webform_key']+field['#options']+WebformID+"'>"+value+"</label>";
        output += "<input class='field input' id='"+field['#webform_key']+field['#options']+WebformID+"' name='"+field['#webform_key']+"' type='checkbox' value='"+key+"' "+is_required+">";
        output += "</div>";
     });

     output += "<div class='error'></div>";
     output += "</div>";


   }

   if (field['#type'] == 'webform_actions'){
     output += "<button class='btn webformPostMeNow' type='button' id='submitButton-"+WebformID+"'>"+field['#submit__label']+"</button>";

   }
   return output;

 };

 function postCiviCrmForm(WebformWrapper){

   var endpointPost = $(WebformWrapper).attr('data-post-url');
   var sendSMS =$(WebformWrapper).attr('data-sendsms');
   var endpointPostSMS = '/sendsms.php';
   var form_id = $(WebformWrapper).attr('data-form-id');
   var jsonArray = {};
   if (debug){
     console.log('endpointPost',endpointPost);
     console.log('form_id',WebformWrapper);
     console.log('sendSMS',sendSMS);
  }

   jsonArray['webform_id'] = form_id;
   if (debug){
     console.log('jsonObj',jsonArray);
   }

   $( WebformWrapper+" .form-item" ).each(function( ) {

     if ($(this).hasClass('multiplevalues')){
       var values = [];

       i = 0;
       $(this).find('input').each(function( ) {
         if ($(this).is(':checked')){
           newvalue = $(this).val();
           if (debug){
             console.log(newvalue);
          }
           if (newvalue.lenght != 0){
              values[i] = newvalue;
              i++;
           }

         }

       });
       //values = values.slice(0,-1);
       if (debug){
         console.log('multiplevalues values',values);
       }

       if (values){
         name = $(this).find('input').attr('name');
         value = values;
       }
       if (debug){
         console.log('multiplevalues values',value);
       }



       if (debug){
         console.log($(this),'multiplevalues');
       }

     }else{
      console.log($(this),'single value');
       name = $(this).find('input').attr('name');
       value = $(this).find('input').val();
     }

     if ( name != 'undefined'){
       jsonArray[name] = value;
     }
   });

   if (debug){
     console.log('jsonArray',jsonArray);
   }

   json = JSON.stringify(jsonArray);
   //$(WebformWrapper).append(json);

   if (debug){
     console.log('json',json);
   }

   var request = $.ajax({
     url: endpointPost,
     headers: {
       'Accept': 'application/json',
       'Content-Type': 'application/json'
     },
     method: "POST",
     dataType: "json",
     data: json,
   });

   request.done(function( msg ) {
     if (debug){
       console.log('done',msg)
    }


    let matomo_formname = 'webform-'+form_id;
    console.log('matomo_formname:', matomo_formname);

    _paq.push(['trackEvent', 'Interaction', 'Invia Form', matomo_formname]);



     $(WebformWrapper).html('<h2>Grazie!</h2><h3>Ti abbiamo inviato un promemoria.</h3>');

     // Send SMS
     if (sendSMS == 'true'){
       $.post(endpointPostSMS, jsonArray, function(result){
        if (debug){
          console.log(result);
        }
      });
      console.log('formvent SMS');
      _paq.push(['FormAnalytics::trackFormConversion', '{formName}', 'webform-5x1000sms']);
     }
     else{
       console.log('formvent email');
       _paq.push(['FormAnalytics::trackFormConversion', '{formName}', 'webform-5x1000mail']);

     }

   });

   request.fail(function( jqXHR, textStatus ) {
     if (debug){
       console.log( "Request failed: " + textStatus );
       console.log( "Request failed: " + jqXHR );
       $(WebformWrapper).html('<h2>Ci spiace, si è verificato un errore inatteso.</h2><h3>Riprova più tardi</h3>');
    }
   });

 }

 function checkRequiredFields(form) {

   $( form ).each(function( index ) {
     if (debug){
       var formClass = $(this).attr('class');
       console.log('checkRequiredFields - Class: ',formClass);
       console.log('checkRequiredFields - Argument',form);
     }

     $(this).find('button.webformPostMeNow').addClass('disabled');
     $(this).find('button.webformPostMeNow').prop("disabled",true);

     var can_send = true;
     var allRequiredFields = [];

     $(this).find('input').each(function() {

       if ($(this).prop('required')){
         name = $(this).attr('name');
         type = $(this).attr('type');
         value = $(this).val();

         if (debug){
           console.log('['+form+'] ['+type+'] '+name+': ', value);
         }

         $(this).parents('.form-item').find('.error').html('');

         if (type=='checkbox'){
           if ($(this).is(":checked"))
             {
               $(this).parents('.form-item').find('.error').html('');
             }
           else{
             $(this).parents('.form-item').find('.error').html('Questo campo è obbligatorio');
             can_send = false;
           }
         }

         if (value != null && value != '' ){
           allRequiredFields[name] = true;

           if (type=='email'){

             var EmailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
             if('regex',EmailRegex.test(value)){

             }
             else{
               $(this).parents('.form-item').find('.error').html('L\'indirizzo email inserito non è valido.');
               can_send = false;
             }

           }
         }
         else{
           allRequiredFields[name] = false;
           $(this).parents('.form-item').find('.error').html('Questo campo è obbligatorio');
           can_send = false;

         }
       }else{
         required = false;
       }

     });


     if (debug){
       console.log('cansend:', can_send);
     }

     if (can_send){
       $(this).find('button.webformPostMeNow').removeClass('disabled');
       $(this).find('button.webformPostMeNow').prop("disabled",false);
     }
   });



 }


});
