let vm = new Vue({
  el: '#page',
  data: {
    preload: true,
    touchDev: false,
    scrollValue: null,
    headerBg: null,
    scrollTo: null,
    menu: false,
    menuClass: '',
    navClass: '',
    sharing: false,
    validField: false,
    tooltip: false,
    errors: {
      name: null,
      surname: null,
      email: null,
      tel: null,
      toggle2: null
    }
  },
  methods: {

    setTransitions(){

      this.preload = false;

      setTimeout(function(){
        document.querySelectorAll('.logo')[0].classList.add("enable");
      }, 400);

      setTimeout(function(){
        document.querySelectorAll('.intro')[0].classList.add("enable");
      }, 400);

      setTimeout(function(){
        document.querySelectorAll('.more')[0].classList.add("enable");
      }, 600);

      setTimeout(function(){
        document.querySelectorAll('.nav')[0].classList.add("enable");
      }, 900);

      setTimeout(function(){
        document.querySelectorAll('.intro')[0].classList.add("enable");
      }, 800);

      setTimeout(function(){
        document.querySelectorAll('.slideshow')[0].classList.add("enable");
      }, 900);

      setTimeout(function(){
        document.querySelectorAll('.reminder')[0].classList.add("enable");
      }, 1200);

      // Viewport

      for (let i = 0, el; el = document.querySelectorAll('.box')[i]; i++) {
        if (el.getBoundingClientRect().top < (document.documentElement.clientHeight - 10)) {
          el.classList.add("enable");
        }
      }

      window.addEventListener('scroll', function() {
        for (let i = 0, el; el = document.querySelectorAll('.box')[i]; i++) {
          if (el.getBoundingClientRect().top < (document.documentElement.clientHeight - 200)) {
            el.classList.add("enable");
          }
        }
      });

    },

    setParallax(){

      let ticking = false;
      let self    = this;
      let nav     = document.querySelectorAll('.nav__item');

      let offset;

      if (document.querySelectorAll('.menu')[0].offsetWidth > 0 && document.querySelectorAll('.menu')[0].offsetHeight > 0) {
        offset = 165;
      } else {
        offset = 120;
      }

      self.scrollValue = document.body.scrollTop || document.documentElement.scrollTop;
      if (self.scrollValue > 60) {
        self.headerBg = 'header--bg';
        self.scrollTo = 'scrolltop';
      } else {
        self.headerBg = '';
        self.scrollTo = '';
      }

      // Header

      window.addEventListener('scroll', function(e) {
        self.scrollValue = document.body.scrollTop || document.documentElement.scrollTop;
        if (!ticking) {
          window.requestAnimationFrame(function() {
            if (self.scrollValue > 60) {
              self.headerBg = 'header--bg';
              self.scrollTo = 'scrolltop';
            } else {
              self.headerBg = '';
              self.scrollTo = '';
            }

            ticking = false;

          });
          ticking = true;
        }

        for (let i = 0, el; el = document.querySelectorAll('.scroll-item')[i]; i++) {
          if (el.getBoundingClientRect().top < offset && el.getBoundingClientRect().bottom > offset) {
            nav[i].classList.add("active");
          } else {
            nav[i].classList.remove("active");
          }
        }

      });

    },

    setScroll(){
      let scroll = new SmoothScroll('.header a[href*="#"]', {
        speed: 1000,
        speedAsDuration: true,
        offset: function (anchor, toggle) {
          if (document.querySelectorAll('.menu')[0].offsetWidth > 0 && document.querySelectorAll('.menu')[0].offsetHeight > 0) {
            return 135;
          } else {
            return 90;
          }
        },
        updateURL: false,
        popstate: false
      });
    },

    setSlideshows(){

      let slideshow = new Swiper('.slideshow__aux', {
        direction: 'horizontal',
        slidesPerView: 1,
        speed: 500,
        loop: true,
        autoplay: {
          delay: 3000,
        }
      })

      let stories = new Swiper('.about-us__stories__aux', {
        direction: 'horizontal',
        slidesPerView: 3,
        breakpoints: {
          900: {
            slidesPerView: 1
          }
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        }
      })

    },

    copyToClipboard(text){

      let self = this;

      if (!navigator.userAgent.match(/ipad|ipod|iphone/i)) {
        const el = document.createElement('textarea');
        el.classList.add("hidden");
        el.value = text;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        this.tooltip = true;

        setTimeout(() => {
          self.tooltip = false;
        }, 3000);
      }

    },

    setTab(el){

      for (let i = 0, el; el = document.querySelectorAll('.how-to__content li')[i]; i++) {
        el.classList.remove("active");
      }

      for (let i = 0, el; el = document.querySelectorAll('.how-to__items li')[i]; i++) {
        el.classList.remove("active");
      }

      document.querySelectorAll('.how-to__content li')[el].classList.add("active");
      document.querySelectorAll('.how-to__items li')[el].classList.add("active");

    },

    toggleMenu(){
      this.menu = !this.menu;
      if (this.menu) {
        this.menuClass = 'active';
        this.navClass = 'active';
      } else {
        this.menuClass = '';
        this.navClass = '';
      }
    },

    showSharing(){
      this.sharing = true;
    },

    hideSharing(){
      this.sharing = false;
    },

    validateEmail() {

      if (!this.reminder.email) {
        this.errors.email = 'Obbligatorio';
      } else if (!this.validEmail(this.reminder.email)) {
        this.errors.email = 'Inserisci un indirizzo email valido';
      } else {
        this.errors.email = null;
        this.validField = true;
      }
    },

    validateTel() {
      if (!this.reminder.tel) {
        this.errors.tel = 'Obbligatorio';
      } else {
        this.errors.tel = null;
      }
    },

    validateName() {
      if (!this.reminder.name) {
        this.errors.name = 'Obbligatorio';
      } else {
        this.errors.name = null;
      }

    },

    validateSurname() {
      if (!this.reminder.surname) {
        this.errors.surname = 'Obbligatorio';
      } else {
        this.errors.surname = null;
      }
    },

    validEmail: function(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },

    validateToggle2(){
      let self = this;
      setTimeout(function(){
        if (!self.reminder.toggle2) {
          self.errors.toggle2 = 'Obbligatorio';
        } else {
          self.errors.toggle2 = null;
        }
      }, 10);
    },

    toggleForm(el) {
      if (el == 'tel') {
        this.reminder.def = false;
      } else if (el == 'email') {
        this.reminder.def = true;
      }
    },

    sendReminder(){

      let self = this;

      if (self.reminder.form) {


        if (self.reminder.def) {
          self.validateEmail();
          if (this.validField) {
            this.sendReminderValidate();
          }
        } else {
          self.validateTel();
          if (this.reminder.tel) {
            this.sendReminderValidate();
          }
        }

      } else {

        self.validateName();
        self.validateSurname();
        self.validateToggle2();
        if (self.reminder.def) {
          self.validateTel();
          if (this.reminder.name && this.reminder.surname && this.reminder.toggle2 && this.reminder.tel) {
            this.sendReminderValidate();
          }
        } else {
          self.validateEmail();
          if (this.reminder.name && this.reminder.surname && this.reminder.toggle2 && this.validField) {
            this.sendReminderValidate();
          }
        }

      }

    },

    sendReminderValidate(){

      let self = this;
      self.reminder.loading = true;

      setTimeout(function(){

        fetch(self.reminder.url, {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            data: {
              id: self.reminder.id,
              name: self.reminder.name,
              surname: self.reminder.surname,
              email: self.reminder.email,
              tel: self.reminder.tel,
              toggle2: self.reminder.toggle2
            }
          })
        }).then(function(response) {

          self.reminder.loading = false;

          if (self.reminder.form) {
            self.reminder.form = false;
            self.reminder.form2 = true;
          } else {
            self.reminder.message = true
          }

        }).catch(function(err) {
          console.log(err)
        });

        // self.reminder.loading = false;
        // if (self.reminder.form) {
        //   self.reminder.form = false;
        //   self.reminder.form2 = true;
        // } else {
        //   self.reminder.message = true
        // }

      }, 1000);
    }

  },
  mounted() {
    this.setTransitions();
    this.setParallax();
    this.setScroll();
    this.setSlideshows();
    document.querySelectorAll('.how-to__content li')[0].classList.add("active");
    document.querySelectorAll('.how-to__items li')[0].classList.add("active");
  }
})
