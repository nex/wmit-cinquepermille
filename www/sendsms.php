<?php
// Include configurations
include_once 'config.php';

// Get POST data
$postedData = $_POST;

$phone = $_POST['civicrm_1_contact_1_phone_phone'];
$validtoken =  $_POST['token'];

$to = '+39'.$phone; //Il numero di cellulare del destinatario deve contente il "+" iniziale

// Get Tokens
$tokens = explode(PHP_EOL, file_get_contents($tokenfile));

// Check Tokens
foreach ($tokens as $token_key => $token_value) {
  // echo "Valid token: ".$validtoken."  -  Hashed token: ".md5($token_value) ."\n";

  // Check if valid token exists
  if ( md5($token_value) == $validtoken){
    unset($tokens[$token_key]);

    $senddata = getSendData($from, $to , $msg);
    print_r($senddata);

    // Send SMS
    $res = sendSMS($volauser, $volapwd, $senddata, "TR45GDLBO730HDUIEQJ5");

    print_r($res);
    exit;
  }
  else{
    echo "Token non trovato\n";
  }
}

$newfile = '';
foreach ($tokens as $token_key => $token_value) {
  $newfile .= $token_value.PHP_EOL;
}

file_put_contents($tokenfile, $newfile);


#
# Funzione che restituisce il contenuto del parametro SENDATA opportunamente
# valorizzato.
#
function getSendData($from, $to, $msg) {
    $res = "0\t$from\t$to\t$msg\t0000-00-00\t00:00\r\n";
    return $res;
}

#
# Funzione che effettua l'invio dei dati a gateway.
# Viene restituito un array associativo:
# 'CODE_RESPONSE'=> codice esito comando,
# 'CID'=> identificativo invio lato cliente,
# 'ORDERID'=> numero ordine relativo all'invio,
# 'TO_ERRORO_LIST' => elenco numeri destinatari non accettati dal gateway
# Esempio:
# Array
#(
# [CODE_RESPONSE] => 01
# [CID] => 0
# [ORDERID] => Array
# (
# [0] => 17549187
# )
#
# [TO_ERRORO_LIST] => Array
#(
# [0] =>
#
# )
#
#)
function sendSMS($volauser, $volapwd, $senddata, $serial, $test = 0) {
    $sito_web = "sms.vola.it";
    $porta = 80;
    $script = "/cgi/volasms_gw_plus2.php";
    $ok = false;
    $fs = fsockopen($sito_web, $porta);

    if ($fs) {
        $header = "";
        $header = $header . "POST $script HTTP/1.1\r\n";
        $header = $header . "Host: $sito_web\r\n";
        $header = $header . "Content-type: application/x-www-form-urlencoded\r\n";
        $volauser = md5($volauser);
        $volapwd = md5($volapwd);
        $senddata = rawurlencode($senddata);
        $str_data = "TEST=" . $test . "&UID=" . $volauser . "&PWD=" . $volapwd .
                "&CMD=14&SENDDATA=" . $senddata . "&SERIAL=" . $serial;
        $length = strlen($str_data);
        $header = $header . "Content-length: $length\r\n\r\n";
        $header = $header . $str_data;

        fputs($fs, $header);
        $continue = true;
        while ($continue) {
            $response = fgets($fs);
            if (preg_match("/[0-9]/", $response)) {
                $continue = false;
                $code = substr($response, 0, 2);
                $tmp = explode(" ", $response);
                $params = $tmp[1];
                $tmp = explode(";", $params);
                $cid = $tmp[0];
                $lst_orderid = explode(",", $tmp[1]);
                $lst_toerror = explode(",", $tmp[2]);
                $res = array('CODE_RESPONSE' => $code,
                    'CID' => $cid, 'ORDERID' => $lst_orderid,
                    'TO_ERRORO_LIST' => $lst_toerror);
            }
        }
    }
    return $res;
}

?>
